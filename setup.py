#!/usr/bin/env python

"""A setuptools based setup module.

See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

import io
import os

from setuptools import setup, find_packages

MODULE_NAME = 'package_skeleton'

requires = [
    'click==6.7'
]

test_requirements = ['pytest']

# long description
def read(*filenames, **kwargs):
    encoding = kwargs.get('encoding', 'utf-8')
    sep = kwargs.get('sep', '\n')
    buf = []
    for filename in filenames:
        with io.open(filename, encoding=encoding) as f:
            buf.append(f.read())
    return sep.join(buf)


setup_dict = dict(
    name=MODULE_NAME,
    version="0.1.0",
    description='Minimal structure for python package, including gitlab CI, pipenv and others.',
    url='https://gitlab.com/pacm/python-package-skeleton',
    author='Paulo Martinez',
    author_email='',
    packages=find_packages(),
    install_requires=requires,
    include_package_data=True,
    entry_points='''
      [console_scripts]
      package_skeleton={}.cli.base:entry_point
      '''.format(MODULE_NAME),
    scripts=[
        os.path.join(MODULE_NAME, 'esp_idf/gen_esp32part.py'),
    ],
    long_description=read('README.md'),
    tests_require=test_requirements
)

if __name__ == '__main__':
    setup(**setup_dict)
