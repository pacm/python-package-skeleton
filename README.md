# Package Skeleton

[![pipeline status](https://gitlab.com/pacm/python-package-skeleton/badges/master/pipeline.svg)](https://gitlab.com/pacm/python-package-skeleton/commits/master)

This project uses [pipenv](https://pipenv.readthedocs.io) to manage its dependencies
and Python environment. It is a good idea to have it installed in your system.


## Installation

This project requires [pipenv](https://pipenv.readthedocs.io) as pre-requisite.
Afther that, clone the repository and:

```bash
cd <clone_folder>
make install
```

In order to activate the virtual environment:

```bash
pipenv shell
```

Or to run a specific command without activating it:

```bash
pipenv run <cmd>
```

For instance:

```bash
pipenv run package_skeleton --help
```

### Development

To install this in a development mode with the extra dependencies, do:

```bash
make develop
```

### Command line interface

To learn how to use this CLI, run: `package_skeleton --help`.
