.PHONY: help clean clean-pyc clean-build clean-pyenv list release_package release_exec sdist install deps dev_deps develop tag

project-name = package_skeleton

version-var := "__version__ = "
version-string := $(shell grep $(version-var) $(project-name)/version.py)
version := $(subst __version__ = ,,$(version-string))

help:
	@echo "clean-build - remove build artifacts"
	@echo "clean-pyc - remove Python file artifacts"
	@echo "clean-pyenv - remove Python environment"
	@echo "lint - check style with flake8"
	@echo "install - install"
	@echo "develop - install in development mode"
	@echo "deps - install dependencies"
	@echo "dev_deps - install dependencies for development"
	@echo "release_package - package a release and upload to index"
	@echo "release_exec - compile a executable and upload to nexus"
	@echo "tag - create a git tag with current version"

install: deps
	pipenv run python setup.py install

develop: dev_deps
	pipenv run python setup.py develop

deps:
	pipenv install

dev_deps:
	pipenv install --dev

clean: clean-build clean-pyc clean-pyenv

clean-build:
	rm -fr build/
	rm -fr dist/
	rm -fr *.egg-info
	rm -fr *.spec

clean-pyc:
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -rf {} +
	find . -name '*.log*' -delete

clean-pyenv:
	pipenv --rm

lint:
	pipenv run flake8 $(project-name)/

release_package:
	pipenv run python setup.py sdist --formats gztar bdist_wheel
	pipenv run twine upload --repository-url $(index-url) dist/*.whl dist/*.tar.gz

release_exec: compile
	curl -v --user '$(TWINE_USERNAME):$(TWINE_PASSWORD)' --upload-file dist/$(project-name)-exec-* $(artifactory-url)

compile:
	pipenv run pyinstaller --clean --onefile execs/$(project-name).spec

tag:
	@echo "Create git tag v$(version), if not present"
	git rev-parse --verify "v$(version)" || (git tag "v$(version)" && git push --tags)

patch:
	pipenv run bumpversion patch

minor:
	pipenv run bumpversion minor

major:
	pipenv run bumpversion major
