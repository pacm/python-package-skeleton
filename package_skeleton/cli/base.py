import click

from package_skeleton.cli.network import network
from package_skeleton.constants import CONTEXT_SETTINGS


@click.group(context_settings=CONTEXT_SETTINGS, help='package_skeleton CLI commands.')
def entry_point():
    pass


entry_point.add_command(network)
