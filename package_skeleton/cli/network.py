import os

import click

from package_skeleton import log
from package_skeleton.constants import CONTEXT_SETTINGS

logger = log.get_logger(os.path.basename(__file__))


@click.group(context_settings=CONTEXT_SETTINGS, help='Set of subcommands for network operations.')
def network():
    pass


@network.command(context_settings=CONTEXT_SETTINGS)
@click.option('--skip', is_flag=True)
def ip(skip):
    """List the IPs from the current machine

    Examples: \n
    package_skeleton network ip
    """
    from package_skeleton.network import localhost_ip_addresses

    if not skip:
        eth_ip, wlan_ip = localhost_ip_addresses()
        logger.debug("the WiFi IP is {} and the ethernet IP is {}.".format(
            wlan_ip,
            eth_ip
        ))
