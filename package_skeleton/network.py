"""
Function helpers to treat with a GNU/Linux OS network settings.
"""
import os
import shlex
from subprocess import check_output


def localhost_ip_addresses():
    """ Discover the IP addresses of the localhost device in a GNU/Linux OS.

    Returns:
        eth_ip, wlan_ip
    """
    eth_ip, wlan_ip = "", ""

    try:
        wlan_interface = ""
        eth_interface = ""

        for _, dirs, _ in os.walk('/sys/class/net'):
            for interface in dirs:

                if "wl" in interface:
                    wlan_interface = interface

                if "en" in interface or "eth" in interface:
                    eth_interface = interface

        if wlan_interface:
            wlan_ip_cmd = "ip addr show " + wlan_interface
            wlan_ip_out = check_output(shlex.split(wlan_ip_cmd)).decode()

            if 'inet' in wlan_ip_out:
                wlan_ip = wlan_ip_out.split('inet ')[1].split(' ')[0].split('/')[0]

        if eth_interface:
            eth_ip_cmd = "ip addr show " + eth_interface
            eth_ip_out = check_output(shlex.split(eth_ip_cmd)).decode()

            if 'inet' in eth_ip_out:
                eth_ip = eth_ip_out.split('inet ')[1].split(' ')[0].split('/')[0]

    except Exception:
        return eth_ip, wlan_ip
    else:
        return eth_ip, wlan_ip
