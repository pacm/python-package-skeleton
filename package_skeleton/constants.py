from pathlib import Path
import tempfile
import logging

LOG_LEVEL = logging.DEBUG

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])

TEMP_DIR = tempfile.mkdtemp(prefix='package_skeleton')

MODULE_BASEDIR = Path(__file__).parent
RESOURCES_DIR = Path(MODULE_BASEDIR) / 'resources'
